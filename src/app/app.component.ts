import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { CurrentUserStoreService } from './common/services/current-user-store.service';
import { GlobalAuthService } from './common/services/global-auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadingRouteConfig: Observable<boolean>;

  constructor(
    private currentUserStoreService: CurrentUserStoreService,
    private globalAuth: GlobalAuthService
  ) {}

  ngOnInit(): void {
    if (this.globalAuth.token) {
      this.currentUserStoreService.initCurrentUser();
    }
  }
}
