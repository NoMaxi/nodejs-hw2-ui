import { Component, OnInit } from '@angular/core';

import { MessageService } from 'primeng/api';

import { User } from '../../../user/interfaces/user';
import { UserService } from '../../../user/services/user.service';
import { CurrentUserStoreService } from '../../../../common/services/current-user-store.service';
import { GlobalAuthService } from '../../../../common/services/global-auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLogin: Boolean;
  user: User;

  constructor(
    private globalAuthService: GlobalAuthService,
    private currentUserStoreService: CurrentUserStoreService,
    private userService: UserService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.currentUserStoreService.userWatcher.subscribe((user: User) => {
      if (user._id) {
        this.user = user;
        this.isLogin = true;
      } else {
        this.isLogin = false;
      }
    });
  }

  userLogout(): void {
    this.userService.logout();
    this.messageService.add({
      severity: 'info',
      summary: 'Logout',
      detail: 'You are logged out'
    });
  }
}
