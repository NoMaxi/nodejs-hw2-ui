export interface LoginResponse {
  jwt_token?: string,
  message?: string
}
