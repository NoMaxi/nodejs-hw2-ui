import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { Credentials } from '../../user/interfaces/credentials';
import { LoginResponse } from '../interfaces/login-response';
import { RegisterResponse } from '../interfaces/register-response';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiUrl: string = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  register(credentials: Credentials): Observable<RegisterResponse> {
    return this.http.post<RegisterResponse>(`${this.apiUrl}/auth/register`, credentials)
      .pipe(map((res) => {
          if (res.message !== "Success") {
            console.error(res.message);
          }
          return res;
        })
      );
  }

  login(credentials: Credentials): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${this.apiUrl}/auth/login`, credentials)
      .pipe(map((res) => {
          if (!res.jwt_token) {
            console.error(res.message);
          } else {
            localStorage.setItem('jwt_token', `JWT ${res.jwt_token}`);
          }
          return res;
        })
      );
  }
}
