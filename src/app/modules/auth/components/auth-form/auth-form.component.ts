import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { MessageService } from 'primeng/api';

import { LoginResponse } from '../../interfaces/login-response';
import { RegisterResponse } from '../../interfaces/register-response';
import { GlobalAuthService } from '../../../../common/services/global-auth.service';
import { CurrentUserStoreService } from '../../../../common/services/current-user-store.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.css']
})
export class AuthFormComponent implements OnInit {
  authForm: FormGroup;

  constructor(
    private router: Router,
    private globalAuthService: GlobalAuthService,
    private currentUserStoreService: CurrentUserStoreService,
    private authService: AuthService,
    private messageService: MessageService
  ) {
  }

  ngOnInit(): void {
    this.authForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ])
    });
  }

  onRegisterSubmit(): void {
    this.authService.register({ ...this.authForm.value })
      .subscribe((res: RegisterResponse) => {
        if (res.message === 'Success') {
          this.messageService.add({
            severity: 'success',
            summary: 'Registration success',
            detail: 'Your account has been successfully registered'
          });
        }
      }, err => {
        console.error(err);
        this.messageService.add({
          severity: 'error',
          summary: 'Registration fail',
          detail: err.error.message
        });
      });
  }

  onLoginSubmit(): void {
    this.authService.login({ ...this.authForm.value })
      .subscribe((res: LoginResponse) => {
        if (res.jwt_token) {
          this.currentUserStoreService.initCurrentUser();
          this.router.navigate(['/users/me']);
          this.messageService.add({
            severity: 'success',
            summary: 'Login success',
            detail: 'You have successfully logged in'
          });
        }
      }, err => {
        console.error(err);
        this.authForm.patchValue({ password: '' });
        this.messageService.add({
          severity: 'error',
          summary: 'Login fail',
          detail: err.error.message
        });
      });
  }

  onSubmit(event): void {
    if (this.authForm.invalid) {
      return console.error('Authorization form validation error');
    }

    const btn = event.submitter.name;
    if (btn === 'login-btn') {
      this.onLoginSubmit();
    } else {
      this.onRegisterSubmit();
    }
  }
}
