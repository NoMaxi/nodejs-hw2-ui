import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { UserRoutingModule } from './user-routing.module';
import { ProfileComponent } from './pages/profile/profile.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { ProfileControlsComponent } from './components/profile-controls/profile-controls.component';
import { ChangePasswordFormComponent } from './components/change-password-form/change-password-form.component';
import { NotesComponent } from './pages/notes/notes.component';
import { NotesListComponent } from './components/notes-list/notes-list.component';
import { NotesItemComponent } from './components/notes-item/notes-item.component';
import { AddNoteFormComponent } from './components/add-note-form/add-note-form.component';
import { UserService } from './services/user.service';
import { NotesService } from './services/notes.service';

@NgModule({
  declarations: [
    ProfileComponent,
    UserInfoComponent,
    ProfileControlsComponent,
    ChangePasswordFormComponent,
    NotesComponent,
    NotesListComponent,
    NotesItemComponent,
    AddNoteFormComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule
  ],
  providers: [
    UserService,
    NotesService
  ]
})
export class UserModule { }
