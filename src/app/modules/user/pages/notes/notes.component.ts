import { Component, OnInit } from '@angular/core';

import { MessageService } from 'primeng/api';

import { Note } from '../../interfaces/note';
import { NotesService } from '../../services/notes.service';
import { NotesStoreService } from '../../../../common/services/notes-store.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  userNotes: Note[];

  constructor(
    private messageService: MessageService,
    private notesService: NotesService,
    private notesStoreService: NotesStoreService
  ) {  }

  ngOnInit(): void {
    this.getNotes();
    this.notesStoreService.notesWatcher.subscribe((notes: Note[]) => {
      if (notes) {
        this.userNotes = notes;
      }
    });
  }

  getNotes(): void {
    this.notesService.getUserNotes()
      .subscribe((notes: Note[]) => {
        if (notes) {
          this.userNotes = notes;
        }
      },err => {
        console.error(err);
        this.messageService.add({
          severity: 'error',
          summary: 'Getting notes error',
          detail: err.error.message
        });
      });
  }
}
