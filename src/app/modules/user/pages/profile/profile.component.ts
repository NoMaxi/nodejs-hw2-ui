import { Component, OnInit } from '@angular/core';

import { MessageService } from 'primeng/api';

import { UserService } from '../../services/user.service';
import { CurrentUserStoreService } from '../../../../common/services/current-user-store.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User;

  constructor(
    private messageService: MessageService,
    private userService: UserService,
    private currentUserStoreService: CurrentUserStoreService,
  ) { }

  ngOnInit(): void {
    this.getUser();
    this.currentUserStoreService.userWatcher.subscribe((user: User) => {
      if (user._id) {
        this.user = user;
      }
    });
  }

  getUser(): void {
    this.userService.getUser().subscribe((user: User) => {
      if (user._id) {
        this.user = user;
      }
    }, err => {
      console.error(err);
      this.messageService.add({
        severity: 'error',
        summary: 'Getting user error',
        detail: err.error.message
      });
    });
  }
}
