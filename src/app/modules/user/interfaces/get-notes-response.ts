import { Note } from './note';

export interface GetNotesResponse {
  notes: Note[]
}
