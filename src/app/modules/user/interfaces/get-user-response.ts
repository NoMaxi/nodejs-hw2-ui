import { User } from './user';

export interface GetUserResponse {
  user: User
}
