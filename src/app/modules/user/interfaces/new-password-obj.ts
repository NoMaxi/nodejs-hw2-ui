export interface NewPasswordObj {
  oldPassword: string,
  newPassword: string
}
