import { Note } from './note';

export interface GetNoteResponse {
  note: Note;
}
