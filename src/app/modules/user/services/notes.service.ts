import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';
import { GetNotesResponse } from '../interfaces/get-notes-response';
import { GetNoteResponse } from '../interfaces/get-note-response';
import { SuccessHttpResponse } from '../../../common/interfaces/success-http-response';
import { map } from 'rxjs/operators';
import { Note } from '../interfaces/note';
import { NotesStoreService } from '../../../common/services/notes-store.service';

@Injectable({
  providedIn: 'root'
})
export class NotesService {
  private apiUrl: string = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private notesStoreService: NotesStoreService
  ) { }

  getUserNotes(): Observable<Note[]> {
    return this.http.get<GetNotesResponse>(`${this.apiUrl}/notes`)
      .pipe(map((res: GetNotesResponse) => {
          if (res.notes) {
            this.notesStoreService.data = res.notes;
          }

          return res.notes;
        })
      );
  }

  getNoteById(id: string): Observable<Note> {
    return this.http.get<GetNoteResponse>(`${this.apiUrl}/notes/${id}`)
      .pipe(map((res: GetNoteResponse) => {
          if (res.note._id) {
            return res.note;
          }
        })
      );
  }

  addNote(text: string): Observable<SuccessHttpResponse> {
    return this.http.post<SuccessHttpResponse>(`${this.apiUrl}/notes`, { text })
      .pipe(map((res: SuccessHttpResponse) => {
          if (res.message === 'Success') {
            this.notesStoreService.initUserNotes();
          }

          return res;
        })
      );
  }

  updateNoteTextById(id: string, text: string): Observable<SuccessHttpResponse> {
    return this.http.put<SuccessHttpResponse>(`${this.apiUrl}/notes/${id}`, { text })
      .pipe(map((res: SuccessHttpResponse) => {
          if (res.message === 'Success') {
            this.notesStoreService.initUserNotes();
          }

          return res;
        })
      );
  }

  toggleNoteCompleteById(id: string): Observable<SuccessHttpResponse> {
    return this.http.patch<SuccessHttpResponse>(`${this.apiUrl}/notes/${id}`, {})
      .pipe(map((res: SuccessHttpResponse) => {
          if (res.message === 'Success') {
            this.notesStoreService.initUserNotes();
          }

          return res;
        })
      );
  }

  deleteNoteById(id: string): Observable<SuccessHttpResponse> {
    return this.http.delete<SuccessHttpResponse>(`${this.apiUrl}/notes/${id}`)
      .pipe(map((res: SuccessHttpResponse) => {
          if (res.message === 'Success') {
            this.notesStoreService.initUserNotes();
          }

          return res;
        })
      );
  }
}
