import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { SuccessHttpResponse } from '../../../common/interfaces/success-http-response';
import { GetUserResponse } from '../interfaces/get-user-response';
import { User } from '../interfaces/user';
import { NewPasswordObj } from '../interfaces/new-password-obj';
import { CurrentUserStoreService } from '../../../common/services/current-user-store.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiUrl: string = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private currentUserStoreService: CurrentUserStoreService
  ) { }

  getUser(): Observable<User> {
    return this.http.get<GetUserResponse>(`${this.apiUrl}/users/me`)
      .pipe(map((res: GetUserResponse) => {
          if (res.user._id) {
            this.currentUserStoreService.info = res.user;
          }

          return res.user;
        })
      );
  }

  changePassword(newPasswordObj: NewPasswordObj): Observable<SuccessHttpResponse> {
    return this.http.patch<SuccessHttpResponse>(`${this.apiUrl}/users/me`, newPasswordObj)
      .pipe(map((res) => {
          if (res.message === 'Success') {
            this.logout();
          }

          return res;
        })
      );
  }

  deleteUser(): Observable<SuccessHttpResponse> {
    return this.http.delete<SuccessHttpResponse>(`${this.apiUrl}/users/me`)
      .pipe(map((res) => {
          if (res.message === 'Success') {
            this.logout();
          }

          return res;
        })
      );
  }

  logout() {
    localStorage.removeItem('jwt_token');
    this.currentUserStoreService.info = {};
  }
}
