import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { MessageService } from 'primeng/api';

import { NotesService } from '../../services/notes.service';
import { SuccessHttpResponse } from '../../../../common/interfaces/success-http-response';

@Component({
  selector: 'app-add-note-form',
  templateUrl: './add-note-form.component.html',
  styleUrls: ['./add-note-form.component.css']
})
export class AddNoteFormComponent implements OnInit {
  addNoteForm: FormGroup;

  constructor(
    private messageService: MessageService,
    private notesService: NotesService
  ) { }

  ngOnInit(): void {
    this.addNoteForm = new FormGroup({
      text: new FormControl('', Validators.required)
    });
  }

  onSubmit(): void {
    if (this.addNoteForm.invalid) {
      return console.error('Add note form validation error');
    }

    this.notesService.addNote(this.addNoteForm.value.text)
      .subscribe((res: SuccessHttpResponse) => {
        if (res.message === 'Success') {
          this.addNoteForm.patchValue({ text: '' });
          this.messageService.add({
            severity: 'success',
            summary: 'Add note success',
            detail: 'New note has been added successfully'
          });
        }
      }, err => {
        console.error(err);
        this.messageService.add({
          severity: 'error',
          summary: 'Add note fail',
          detail: err.error.message
        });
      })
  }
}
