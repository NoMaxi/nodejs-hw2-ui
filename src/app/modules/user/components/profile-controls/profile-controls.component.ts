import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MessageService } from 'primeng/api';

import { User } from '../../interfaces/user';
import { SuccessHttpResponse } from '../../../../common/interfaces/success-http-response';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-profile-controls',
  templateUrl: './profile-controls.component.html',
  styleUrls: ['./profile-controls.component.css']
})
export class ProfileControlsComponent implements OnInit {
  @Input() user: User;

  constructor(
    private router: Router,
    private userService: UserService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void { }

  deleteProfile(): void {
    if (confirm('Are you sure you want to delete your profile?')) {
      this.userService.deleteUser().subscribe((res: SuccessHttpResponse) => {
        if (res.message === 'Success') {
          this.router.navigate(['/auth']);
          this.messageService.add({
            severity: 'success',
            summary: 'Profile deletion success',
            detail: 'Your profile has been successfully deleted'
          });
        }
      }, err => {
        console.error(err);
        this.messageService.add({
          severity: 'error',
          summary: 'Profile deletion fail',
          detail: err.error.message
        });
      });
    }
  }
}
