import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { MessageService } from 'primeng/api';

import { SuccessHttpResponse } from '../../../../common/interfaces/success-http-response';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-change-password-form',
  templateUrl: './change-password-form.component.html',
  styleUrls: ['./change-password-form.component.css']
})
export class ChangePasswordFormComponent implements OnInit {
  changePasswordForm: FormGroup;

  constructor(
    private router: Router,
    private userService: UserService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.changePasswordForm = new FormGroup({
      oldPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
      newPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ])
    });
  }

  onSubmit(): void {
    if (this.changePasswordForm.invalid) {
      return console.error('Change password form validation error');
    }

    this.userService.changePassword({ ...this.changePasswordForm.value })
      .subscribe((res: SuccessHttpResponse) => {
        if (res.message === 'Success') {
          this.router.navigate(['/auth']);
          this.messageService.add({
            severity: 'success',
            summary: 'Change password success',
            detail: 'Your profile password has been successfully changed'
          });
        }
      }, err => {
        console.error(err);
        this.changePasswordForm.patchValue({ oldPassword: '', newPassword: '' });
        this.messageService.add({
          severity: 'error',
          summary: 'Change password fail',
          detail: err.error.message
        });
      });
  }
}
