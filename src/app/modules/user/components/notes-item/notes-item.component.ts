import { Component, Input, OnInit } from '@angular/core';

import { MessageService } from 'primeng/api';

import { Note } from '../../interfaces/note';
import { SuccessHttpResponse } from '../../../../common/interfaces/success-http-response';
import { NotesService } from '../../services/notes.service';

@Component({
  selector: 'app-notes-item',
  templateUrl: './notes-item.component.html',
  styleUrls: ['./notes-item.component.css']
})
export class NotesItemComponent implements OnInit {
  @Input() note: Note;
  isNoteCompleted: boolean;
  isNoteBeingEdited: boolean = false;
  noteText: string;

  constructor(
    private messageService: MessageService,
    private notesService: NotesService
  ) { }

  ngOnInit(): void {
    this.isNoteCompleted = this.note.completed;
    this.noteText = this.note.text;
  }

  triggerNoteEditing(): void {
    this.isNoteBeingEdited = true;
  }

  cancelNoteEditing() {
    this.isNoteBeingEdited = false;
    this.noteText = this.note.text;
  }

  updateNoteText(): void {
    this.notesService.updateNoteTextById(this.note._id, this.noteText)
      .subscribe((res: SuccessHttpResponse) => {
        if (res.message === 'Success') {
          this.messageService.add({
            severity: 'success',
            summary: 'Note update success',
            detail: 'Note has been successfully updated'
          });
        }
      }, err => {
        console.error(err);
        this.messageService.add({
          severity: 'error',
          summary: 'Note update error',
          detail: err.error.message
        });
      });
  }

  deleteNote(): void {
    if (confirm('Are you sure you want do delete the note?')) {
      this.notesService.deleteNoteById(this.note._id)
        .subscribe((res: SuccessHttpResponse) => {
          if (res.message === 'Success') {
            this.messageService.add({
              severity: 'success',
              summary: 'Note deletion success',
              detail: 'Note has been successfully deleted'
            });
          }
        }, err => {
          console.error(err);
          this.messageService.add({
            severity: 'error',
            summary: 'Note deletion error',
            detail: err.error.message
          });
        });
    }
  }

  toggleNoteComplete(): void {
    this.notesService.toggleNoteCompleteById(this.note._id)
      .subscribe((res: SuccessHttpResponse) => {
        if (res.message === 'Success') {
          this.isNoteCompleted = !this.isNoteCompleted;
          this.messageService.add({
            severity: 'success',
            summary: 'Note complete success',
            detail: 'Note has been successfully completed'
          });
        }
      }, err => {
        console.error(err);
        this.messageService.add({
          severity: 'error',
          summary: 'Note complete error',
          detail: err.error.message
        });
      });
  }
}
