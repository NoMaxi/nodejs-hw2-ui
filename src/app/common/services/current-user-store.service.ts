import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { GetUserResponse } from '../../modules/user/interfaces/get-user-response';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserStoreService {
  private apiUrl: string = environment.apiUrl;
  private currentUser = {};
  private userWatcherSource: BehaviorSubject<any> = new BehaviorSubject(this.info);
  public userWatcher = this.userWatcherSource.asObservable();

  constructor(
    private http: HttpClient
  ) { }

  public get info(): object {
    return this.currentUser;
  }

  public set info(user) {
    this.currentUser = { ...user };
    this.userWatcherSource.next({ ...user });
  }

  public initCurrentUser(): void {
    this.http.get<GetUserResponse>(`${this.apiUrl}/users/me`)
      .subscribe((res: GetUserResponse) => {
        if (res.user._id) {
          this.info = res.user;
        }
      });
  }
}
