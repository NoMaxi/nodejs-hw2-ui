import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { environment } from '../../../environments/environment';
import { Note } from '../../modules/user/interfaces/note';
import { GetNotesResponse } from '../../modules/user/interfaces/get-notes-response';

@Injectable({
  providedIn: 'root'
})
export class NotesStoreService {
  private apiUrl: string = environment.apiUrl;
  private userNotes: Note[];
  private notesWatcherSource: BehaviorSubject<any> = new BehaviorSubject(this.data);
  public notesWatcher = this.notesWatcherSource.asObservable();

  constructor(
    private http: HttpClient
  ) { }

  public get data(): Note[] {
    return this.userNotes;
  }

  public set data(notes: Note[]) {
    this.userNotes = [...notes];
    this.notesWatcherSource.next([...notes]);
  }

  initUserNotes(): void {
    this.http.get<GetNotesResponse>(`${this.apiUrl}/notes`)
      .subscribe((res: GetNotesResponse) => {
        if (res.notes) {
          this.data = res.notes;
        }
      });
  }
}
