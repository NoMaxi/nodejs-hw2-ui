import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class GlobalAuthService {
  private jwtHelper: JwtHelperService = new JwtHelperService();

  constructor() { }

  get isLoggedIn(): boolean {
    return !!localStorage.getItem('jwt_token');
  }

  get token(): string {
    return localStorage.getItem('jwt_token') || '';
  }

  get userId() {
    return this.token ? this.jwtHelper.decodeToken(this.token)._id : null;
  }
}
