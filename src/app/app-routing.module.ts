import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './common/guards/auth.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module')
      .then(module => module.AuthModule)
  },
  {
    path: 'users/me',
    loadChildren: () => import('./modules/user/user.module')
      .then(module => module.UserModule),
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: 'users/me',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'users/me',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
